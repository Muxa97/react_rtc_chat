import React from 'react';
import { Redirect } from 'react-router-dom';

function PrivateComponent(props) {
    if (props.userLoggedIn) {
        return props.children;
    } else {
        return <Redirect to={{ pathname: '/login' }} />;
    }
}

export default PrivateComponent;