import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import FontAwesome from 'react-fontawesome';
import './AppHeader.css';

class AppHeader extends Component {
    constructor(props) {
        super(props);

        this.state = {
            chat_id: props.chat_id,
            username: props.user,
            users_online: props.users_online,
            copied: false,
            showUsers: false,
            leaveChat: props.leaveChat
        }
    }

    componentWillReceiveProps(nextProps, nextContext) {
        const { users_online } = nextProps;

        this.setState(state => ({
            ...state,
            users_online: users_online
        }))
    }

    copyLink = () => {
        const id = this.state.chat_id;

        const el = document.createElement('textarea');
        el.value = id;
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);

        this.setState(state => ({
            ...state,
            copied: true
        }));

        setTimeout(() => {
            this.setState(state => ({
                ...state,
                copied: false
            }));
        }, 2000);
    };

    showUsers = e => {
        const posX = e.nativeEvent.clientX;
        const posY = e.nativeEvent.clientY;
        const listener = () => {
            this.setState(state => ({
                ...state,
                showUsers: false
            }));
            document.removeEventListener('click', listener);
        };

        this.setState(state => ({
            ...state,
            showUsers: !state.showUsers
        }), () => {
            const users = document.querySelector('.users-list');
            if (users) {
                users.style.top = posY + 10 + 'px';
                users.style.left = posX - 80 + 'px';
                document.addEventListener('click', listener);
            } else {
                document.removeEventListener('click', listener);
            }
        });
    };

    render() {
        return (
            <div className="chat_header">
                <header className="title">React RTC Chat</header>
                <div className="room-info">
                    <span>Room #{this.state.chat_id} <FontAwesome name="copy" onClick={this.copyLink}/>
                        { this.state.copied && <span className="hint">Copied id</span> }
                    </span>
                    <span>Users online {this.state.users_online.length}
                    <FontAwesome name="caret-down" onClick={this.showUsers}/>
                        {
                            this.state.showUsers &&
                            <div className="users-list">
                                {this.state.users_online.map((user, i) => (
                                    <span key={i}>{user}</span>
                                ))}
                            </div>
                        }
                    </span>
                </div>
                <div className="user-info">
                    <span>User: {this.state.username}</span>
                    <Link to='/home' className="leave-btn" onClick={this.state.leaveChat}>Leave</Link>
                </div>
            </div>
        )
    }
}

export default AppHeader;