import React, { useState } from 'react';
import './InputMessageForm.css';

function InputMessageForm(props) {
    const [ msgText, onMsgTextInput ] = useState('');
    const user = props.user;
    const sendMessage = (text, user) => {
        const date = (new Date()).toLocaleString();

        const msg = {
            msg_id: Math.random() * 100 + 15,
            username: user,
            date: date,
            text: text
        };

        props.sendMessage(msg);
        onMsgTextInput('');
    };

    return (
        <div className="input-msg-wrapper">
                <textarea className="msg-input" placeholder="Enter your message"
                          onChange={e => onMsgTextInput(e.target.value)} value={msgText} />
            <div className="controls">
                <button onClick={() => sendMessage(msgText, user)}>Send</button>
            </div>
        </div>
    )
}

export default InputMessageForm;