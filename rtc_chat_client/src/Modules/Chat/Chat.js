import React, { Component } from 'react';
import io from 'socket.io-client';

import AppHeader from './AppHeader/AppHeader';
import MessageList from './MessageList/MessageList';
import InputMessageForm from './InputMessageForm/InputMessageForm';
import './Chat.css';

class Chat extends Component {
    constructor(props) {
        super(props);

        const chat_id = props.match.params.id;
        this.socket = null;
        this.state = {
            chat_id: chat_id,
            messages: [],
            users_online: [],
            username: props.user,
            leaveChat: props.leaveChat
        }
    }

    componentDidMount() {
        this.socket = io();

        this.socket.on('connect', () => {
            const data = {
                chat_id: this.state.chat_id,
                username: this.state.username
            };

            this.socket.emit('user:join', JSON.stringify(data));
        });

        this.socket.on('user:join', user => {
            const msg = {};
            msg.username = 'system';
            msg.text = `User ${user} join this chat`;

            this.setState(state => ({
                ...state,
                users_online: [ ...state.users_online, user ],
                messages: [ ...state.messages, msg ]
            }));
        });

        this.socket.on('user:leave', username => {
            const msg = {};
            msg.username = 'system';
            msg.text = `User ${username} leave this chat`;

            this.setState(state => ({
                ...state,
                users_online: state.users_online.filter(user => user !== username),
                messages: [ ...state.messages, msg ]
            }))
        });

        this.socket.on('chat:data', users => {
            const users_online = JSON.parse(users) || [];
            users_online.push(this.state.username);

            this.setState(state => ({
                ...state,
                users_online: users_online
            }))
        });

        this.socket.on('message:new', message => {
            let msg = JSON.parse(message);

            this.setState(state => ({
                ...state,
                messages: [ ...state.messages, msg ]
            }))
        });
    }

    sendMessage = msg => {
        msg.chat_id = this.state.chat_id;
        this.socket.emit('message:send', JSON.stringify(msg));

        this.setState(state => ({
            ...state,
            messages: [ ...state.messages, msg ]
        }))
    };

    render() {
        return (
            <div className="chat-wrapper">
                <AppHeader users_online={this.state.users_online}
                           user={this.state.username} chat_id={this.state.chat_id} leaveChat={() => {
                               const data = {
                                   username: this.state.username,
                                   chat_id: this.state.chat_id
                               };
                               this.socket.emit('user:leave', JSON.stringify(data));
                               this.socket.close();
                               this.state.leaveChat();
                }}/>
                <MessageList messages={this.state.messages}/>
                <InputMessageForm user={this.state.username} sendMessage={this.sendMessage} />
            </div>
        )
    }
}

export default Chat;