import React, { useEffect } from 'react';
import './MessageList.css';

function Message(props) {
    const { msg } = props;

    return (
        <div className={msg.username === 'system' ? 'sys-msg' : 'msg'}>
            {msg.username !== 'system' && <header className="msg-header">{msg.username}</header>}
            <div className="msg-content">{msg.text}</div>
            {msg.username !== 'system' && <footer className="msg-footer">{msg.date.toString()}</footer>}
        </div>
    )
}

function MessageList(props) {
    useEffect(() => {
        const element = document.querySelector('.message-list');
        element.scrollTop = element.scrollHeight;
    });

    return (
        <div className="message-list">
            {props.messages.map(msg => <Message key={msg.msg_id} msg={msg} /> )}
        </div>
    )
}

export default MessageList;