import React from 'react';
import { Link } from 'react-router-dom';

import './HomePage.css';

function HomePage(props) {
    return (
        <div className="home-wrapper">
            <header className="home-header">Hello, {props.username}</header>
            <Link className="home-link" to={`/chat`}>Start new chat</Link>
            <Link className="home-link" to={`/chat/${props.chat_id}`}>Join chat</Link>
            <input className="chat_id-input" type="text"
                   placeholder="Enter chat id" onChange={e => {props.changeChatID(e.target.value)}}
                   value={props.chat_id || ''}/>
        </div>
    )
}

export default HomePage;