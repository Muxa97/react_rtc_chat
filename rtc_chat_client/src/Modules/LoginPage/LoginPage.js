import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import './LoginPage.css';

function LoginPage(props) {
    const [ canLogin, onLoginInput ] = useState(false);
    const [ username, onUsernameInput ] = useState('');

    return (
        <div className="login-wrapper">
            <header className="login-header">Enter your name:</header>
            <input className={`login-input ${canLogin ? '' : 'error'}`}
                   type="text" placeholder="Your name" onChange={e => {
                       onLoginInput(e.target.value.length >= 3);
                       onUsernameInput(e.target.value);
                    }} value={username}/>
            <Link to='/home' className={`login-btn ${canLogin ? '' : 'disabled'}`}
                  onClick={e => {
                      if(canLogin) {
                          props.login(username, props.location.state ? props.location.state.chat_id : null)
                      } else {
                          e.preventDefault();
                      }
                  }}>Login</Link>
        </div>
    )
}

export default LoginPage;