import React, { Component } from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom';

import LoginPage from './Modules/LoginPage/LoginPage';
import HomePage from './Modules/HomePage/HomePage';
import Chat from './Modules/Chat/Chat';
import PrivateComponent from './Modules/PrivateComponent/PrivateComponent';

class App extends Component {
  constructor() {
    super();

    this.state = {
      userLoggedIn: false,
      username: null,
      chat_id: null
    }
  }

  login = username => {
      this.setState(state => ({
          ...state,
          userLoggedIn: true,
          username: username
      }));
  };

  leaveChat = () => {
      this.setState(state => ({
          ...state,
          chat_id: null
      }))
  };

  changeChatID = id => {
      this.setState(state => ({
          ...state,
          chat_id: id
      }));
  };

  generateChatID = () => {
      const id = Math.floor(Math.random() * 100 + 1);

      this.setState(state => ({
          ...state,
          chat_id: id
      }));

      return id;
  };

  render() {
    return (
        <Router>
            <div className="App">
                <Route exact path="/" render={ () => <Redirect to={{pathname: '/login'}} /> } />
                <Route path="/login" render={ props => <LoginPage {...props} login={this.login} />} />

                <Route path="/home" render={ props => <PrivateComponent userLoggedIn={this.state.userLoggedIn}>
                                                        <HomePage {...props} chat_id={this.state.chat_id}
                                                                  username={this.state.username}
                                                                  changeChatID={this.changeChatID}/>
                                                      </PrivateComponent> } />

                <Route path="/chat/:id" render={props => <PrivateComponent userLoggedIn={this.state.userLoggedIn}>
                                                            <Chat user={this.state.username}
                                                                  leaveChat={this.leaveChat} {...props}/>
                                                         </PrivateComponent>}/>

                <Route exact path="/chat" render={ () =>
                    <Redirect to={{pathname: `/chat/${this.generateChatID()}`}} />
                }/>
            </div>
        </Router>
    );
  }
}

export default App;
