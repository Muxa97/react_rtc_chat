const express = require('express');
const app = express();
const http = require('http').createServer(app);
const io = require('socket.io')(http);

app.use(express.static(__dirname + '/rtc_chat_client/build'));

app.get('/', function(req, res) {
    res.sendFile(__dirname + '/rtc_chat_client/build/index.html');
});

app.get('*', function(req, res) {
    res.redirect('/');
});

const rooms = {};

io.on('connection', function(socket){
    socket.on('user:join', data => {
        const { username, chat_id } = JSON.parse(data);
        socket.join(chat_id);

        io.to(socket.id).emit('chat:data', JSON.stringify(rooms[chat_id]));

        if (rooms[chat_id]) {
            rooms[chat_id].push(username);
        } else {
            rooms[chat_id] = [ username ];
        }
        socket.to(chat_id).emit('user:join', username);
    });

    socket.on('user:leave', data => {
        const { username, chat_id } = JSON.parse(data);

        rooms[chat_id] = rooms[chat_id].filter(user => user !== username);
        socket.to(chat_id).emit('user:leave', username);
    });

    socket.on('message:send', msg => {
        const { chat_id } = JSON.parse(msg);
        socket.to(chat_id).emit('message:new', msg);
    });
});



http.listen(process.env.PORT || 3000);